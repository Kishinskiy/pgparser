import psycopg2
import argparse
import pickle


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--host', help='db host ip')
parser.add_argument('--user', help='dbuser')
parser.add_argument('--password', help='dbpassword')
parser.add_argument('--db', help='dbname')
args = parser.parse_args()


conn = psycopg2.connect(dbname=args.db, user=args.user,
                        password=args.password, host=args.host)
cursor = conn.cursor()


cursor.execute('SELECT * FROM city LIMIT 10')
records = cursor.fetchall()

try:
    pickle.dump (records, open("/tmp_repos/data.bin", "wb"))
except Exception as e:
    print(e)
else:
     print(records)
cursor.close()
conn.close()
